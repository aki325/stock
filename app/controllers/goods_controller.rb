class GoodsController < ApplicationController


    def index
        @good = Good.order(created_at: :asc)
    end
    
    def search
        #Viewのformで取得したパラメータをモデルに渡す
        @good = Good.search(params[:search]).paginate(page: params[:page], per_page: 3)
    end
    
    def import
        # fileはtmpに自動で一時保存される
        Good.import(params[:file])
        redirect_to goods_url
    end
    
    def show
        @good = Good.find(params[:id])
    end
    
    def new
        @good = Good.new()
    end
    
    def edit
        @good = Good.find(params[:id])
    end
    
    def create
        @good = Good.new(good_params)
        if @good.save
            redirect_to @good, notice: "商品を登録しました"
        else
            render "new"
        end
    end
    
    def update
        @good = Good.find(params[:id])
        @good.assign_attributes(good_params)
        if @good.save
            redirect_to @good, notice: "商品を登録しました"
        else
            render "new"
        end
    end

    
    def destroy
        @good = Good.find(params[:id])
        @good.destroy
        redirect_to :goods, notice: "商品を削除しました"
    end
    
    def good_params
      params.require(:good).permit(:style_code, :style_name, :color_code, :color_name, :size, :gender)
    end


end
