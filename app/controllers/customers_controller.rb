class CustomersController < ApplicationController

    def index
        @customer = Customer.order(Created_at: :asc)
    end
    
    def import
    # fileはtmpに自動で一時保存される
        Customer.import(params[:file])
        redirect_to customers_url
    end

    def show
        @customer = Customer.find(params[:id])
    end
    
    def new
        @customer = Customer.new()
    end

    def edit
        @customer = Customer.find(params[:id])
    end

    def create
        @customer = Customer.new(customer_params)
		if @customer.save
			redirect_to @customer, notice: "顧客マスタを登録しました"
		else
			render "new"
		end
    end

    def update
        @customer = Customer.find(params[:id])
		@customer.assign_attributes(customer_params)
		if @customer.save
			redirect_to @customer, notice: "顧客マスタを登録しました"
		else
			render "new"
		end
    end

    def destroy
        @customer = Customer.find(params[:id])
        @customer.destroy
        redirect_to :customers, notice: "顧客マスタを削除しました"
    end

    def customer_params
      params.require(:customer).permit(:account_code, :account_name, :shipped_code, :shipped_name, :user_id)
    end

end