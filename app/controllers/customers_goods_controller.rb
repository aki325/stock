class CustomersGoodsController < ApplicationController

    def index
        @customers_good = CustomersGood.order(Created_at: :asc)
    end

    def show
        @customers_good = CustomersGood.find(params[:id])
    end
    
    def new
        @customers_good = CustomersGood.new()
    end

    def edit
        @customers_good = CustomersGood.find(params[:id])
    end

    def create
        @customers_good = CustomersGood.new(customer_good_params)
		if @customers_good.save
			redirect_to @customers_good, notice: "入出庫登録しました"
		else
			render "new"
		end
    end

    def update
        @customers_good = CustomersGood.find(params[:id])
		@customers_good.assign_attributes(customer_good_params)
		if @customers_good.save
			redirect_to @customers_good, notice: "入出庫登録しました"
		else
			render "new"
		end
    end

    def destroy
        @customers_good = CustomersGood.find(params[:id])
        @customers_good.destroy
        redirect_to :customers_goods, notice: "入出庫を削除しました"
    end

    def customer_good_params
      params.require(:customers_good).permit(:amount, :shipped_date, :processing_date)
    end
end