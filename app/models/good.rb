class Good < ApplicationRecord
  has_many :customers_goods
  has_many :customer, through: :customers_goods



  def self.search(search) #ここでのself.はGood.を意味する
    if search
      where('style_code LIKE ? or style_name LIKE ?', "%#{search}%","%#{search}%") #検索とstyle_codeの部分一致を表示。Good.は省略
    else
      all #全て表示。Good.は省略
    end
  end

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      # IDが見つかれば、レコードを呼び出し、見つかれなければ、新しく作成
      good = find_by(id: row["id"]) || new
      # CSVからデータを取得し、設定する
      good.attributes = row.to_hash.slice(*updatable_attributes)
      # 保存する
      good.save
    end
  end

  # 更新を許可するカラムを定義
  def self.updatable_attributes
    ["style_code","style_name","color_code","color_name","size","gender"]
  end

end
