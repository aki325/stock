class Customer < ApplicationRecord
  has_many :customers_goods
  has_many :good, through: :customers_goods
  
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      # IDが見つかれば、レコードを呼び出し、見つかれなければ、新しく作成
      customer = find_by(id: row["id"]) || new
      # CSVからデータを取得し、設定する
      customer.attributes = row.to_hash.slice(*updatable_attributes)
      # 保存する
      customer.save
    end
  end

  # 更新を許可するカラムを定義
  def self.updatable_attributes
    ["account_code","account_name","shipped_code","shipped_name","user_id"]
  end
end
