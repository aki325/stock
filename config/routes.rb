Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "top#index"
  
  resources:goods do
    collection do
      get 'search'
    end
  end
  
  
  
  resources:customers
  resources:customers_goods
  
  devise_for :users, :controllers => {
  :registrations => 'users/registrations'
  }



  Rails.application.routes.draw do
    resources :goods do
      collection { post :import }
    end
  end

  Rails.application.routes.draw do
    resources :customers do
      collection { post :import }
    end
  end

end
