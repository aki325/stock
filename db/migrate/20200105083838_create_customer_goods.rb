class CreateCustomerGoods < ActiveRecord::Migration[5.0]
  def change
    create_table :customers_goods do |t|
      t.integer  :customer_id
      t.integer  :good_id
      t.integer  :amount
      t.date     :dhipped_date
      t.date     :processing_date
      t.timestamps
    end
  end
end
