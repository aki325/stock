class RenameDhippedDateColumnToCustomersGoods < ActiveRecord::Migration[5.0]
  def change
    rename_column :customers_goods, :dhipped_date, :shipped_date
  end
end
