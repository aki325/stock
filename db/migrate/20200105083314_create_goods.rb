class CreateGoods < ActiveRecord::Migration[5.0]
  def change
    create_table :goods do |t|
      t.integer :style_code
      t.string :style_name
      t.integer :color_code
      t.string :color_name
      t.integer :size
      t.string :gender
      t.timestamps
    end
  end
end
