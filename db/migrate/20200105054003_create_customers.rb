class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.integer :shipped_code
      t.string :shipped_name
      t.integer :account_code
      t.string :account_name
      t.integer :user_id

      t.timestamps
    end
  end
end
