class ChangeDatatypeColorCodeOfGoods < ActiveRecord::Migration[5.0]
  def change
      change_column :goods, :color_code, :string
  end
end
